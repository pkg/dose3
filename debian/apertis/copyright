Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: LGPL-3

Files: Copyright
Copyright: 2013-2016, Pietro Abate and Inria.
 2009-2013, Pietro Abate and Université Paris-Diderot.
License: UNKNOWN

Files: debian/*
Copyright: © 2011, 2012 Ralf Treinen, Stefano Zacchiroli
License: LGPL-3+

Files: debian/dose3-copyright
Copyright: Alain Frisch
 2009, Pietro Abate <pietro.abate@pps.jussieu.fr>
License: LGPL-3+

Files: debian/update-cudf-solvers
Copyright: Stefano Zacchiroli 2011, <zack@debian.org>
License: LGPL-3+

Files: doc/*
Copyright: 2010-2012, Pietro Abate
License: GPL-3+ and/or LGPL-3+

Files: doc/manpages/*
Copyright: 2011, Stefano Zacchiroli <zack@debian.org>
 2011, Pietro Abate <pietro.abate@pps.jussieu.fr>
License: LGPL

Files: doc/webpages/*
Copyright: 2008, Alexandre Dupas
License: GPL-2+

Files: doc/webpages/IkiWiki/Plugin/bootmenu.pm
Copyright: 2011, Julian Andres Klode <jak@jak-linux.org>
License: UNKNOWN

Files: header.txt
Copyright: 2009-2012, Pietro Abate <pietro.abate@pps.jussieu.fr>
License: LGPL-3+

Files: scripts/*
Copyright: 2011, OCamlPro SAS *)
License: UNKNOWN

Files: src/*
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
 2009-2011, 2015, Mancoosi Project *)
License: LGPL

Files: src/algo/flatten.ml
Copyright: 2005-2011, Jerome Vouillon
License: UNKNOWN

Files: src/algo/statistics.ml
Copyright: 2008, Stefano Zacchiroli <zack@debian.org> and *)
License: LGPL

Files: src/applications/aptCudf.ml
 src/applications/challenged.ml
 src/applications/outdated.ml
Copyright: no-info-found
License: LGPL

Files: src/applications/debCoinstall.ml
 src/applications/distcheck.ml
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: src/applications/dominators-graph.ml
Copyright: 2011, Pietro Abate <abate@pps.jussieu.fr> *)
 2010, Jaap Boender <boender@pps.jussieu.fr> *)
 2009, Mancoosi Project *)
License: LGPL

Files: src/common/cudfAdd.mli
 src/common/cudfSolver.ml
Copyright: no-info-found
License: LGPL

Files: src/common/edosSolver.ml
 src/common/edosSolver.mli
Copyright: 2005-2009, Jerome Vouillon *)
License: LGPL

Files: src/common/shell_lexer.mll
Copyright: 2015, Mancoosi Project *)
 2015, Johannes Schauer <j.schauer@email.de> *)
License: LGPL

Files: src/common/util.ml
Copyright: 2009, <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: src/deb/architecture.ml
 src/deb/architecture.mli
Copyright: 2010, 2011, Ralf Treinen <ralf.treinen@pps.jussieu.fr> *)
License: LGPL

Files: src/deb/evolution.ml
Copyright: 2011, Pietro Abate *)
 2011, Mancoosi Project *)
License: LGPL

Files: src/deb/packages.ml
 src/deb/release.ml
 src/deb/release.mli
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: src/deb/tests/*
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: src/doseparse/*
Copyright: no-info-found
License: LGPL

Files: src/experimental/attic/outdated-ralf.ml
Copyright: 2010, 2011, Ralf Treinen <treinen@pps.jussieu.fr> *)
License: GPL

Files: src/experimental/attic/strongpred-clustered.ml
 src/experimental/attic/strongpred.ml
Copyright: 2009, Roberto Di Cosmo <roberto@dicosmo.org> *)
 2009, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
 2009, Mancoosi Project *)
License: LGPL

Files: src/experimental/cnftocudf.ml
 src/experimental/depclean.ml
 src/experimental/edsp-cudf.ml
 src/experimental/example.ml
 src/experimental/lintian.ml
 src/experimental/treinen-test.ml
Copyright: no-info-found
License: LGPL

Files: src/experimental/dormcheck.ml
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: src/experimental/scripts/*
Copyright: 2010, Roberto Di Cosmo, Mancoosi Project
License: UNKNOWN

Files: src/experimental/scripts/pipeline/*
Copyright: no-info-found
License: EPL and/or GPL-2 and/or GPL-3

Files: src/experimental/scripts/pipeline/apt-mancoosi
Copyright: 2010, Pietro Abate <pietro.abate@pps.jussieu.fr>
License: GPL-3+

Files: src/experimental/strong-conflicts.ml
Copyright: 2009, 2010, Mancoosi Project *)
 2009, 2010, Jaap Boender *)
License: LGPL

Files: src/experimental/test-solver.ml
Copyright: 2009, Mancoosi Project *)
License: LGPL

Files: src/extra/criteria_lexer.mll
Copyright: 2011, Pietro Abate *)
 2011, Mancoosi Project *)
License: LGPL

Files: src/extra/format822_lexer.mll
Copyright: 2009-2011, Stefano Zacchiroli <zack@pps.jussieu.fr> *)
License: LGPL

Files: src/extra/format822_parser.mly
Copyright: (C/ 2009-2011, Stefano Zacchiroli <zack@pps.jussieu.fr>
License: LGPL-3

Files: src/extra/url.ml
 src/extra/url.mli
Copyright: 2011-2013, Ralf Treinen *)
License: LGPL

Files: src/npm/*
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: src/npm/npmcudf.ml
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
 2009-2011, 2015, Mancoosi Project *)
License: LGPL

Files: src/opam2/*
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: src/opam2/opamcudf.ml
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
 2009-2011, 2015, Mancoosi Project *)
License: LGPL

Files: Copyright doc/webpages/IkiWiki/Plugin/bootmenu.pm scripts/* src/algo/flatten.ml src/experimental/scripts/* tests/* tests/DebianPackages/Sources_20201226T144309Z.xz tests/DebianPackages/debian_20201226T144309Z.cudf.xz tests/DebianPackages/debian_20201226T144309Z.edsp.xz tests/applications/* tests/applications/dose-tests/distcheck_test_success.xz tests/applications/dose-tests/opam_stack_overflow.xz tests/opam/*
Copyright: © 2009-2012 Pietro Abate
License: LGPL-3+

Files: src/opencsw/*
Copyright: no-info-found
License: LGPL

Files: src/versioning/*
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.univ-paris-diderot.fr> *)
 2009, Mancoosi Project *)
License: LGPL

Files: src/versioning/debian.ml
Copyright: 2010, 2011, Ralf Treinen <ralf.treinen@pps.jussieu.fr> *)
License: LGPL

Files: src/versioning/debian.mli
Copyright: 2009-2015, Pietro Abate <pietro.abate@pps.jussieu.fr> *)
License: LGPL

Files: tests/*
Copyright: no-info-found
License: UNKNOWN

Files: tests/DebianPackages/Sources_20201226T144309Z.xz
Copyright: no-info-found
License: UNKNOWN

Files: tests/DebianPackages/debGrep.ml
Copyright: 2021, Johannes Schauer Marin Rodrigues <josch@mister-muffin.de> *)
License: LGPL

Files: tests/DebianPackages/debian_20201226T144309Z.cudf.xz
Copyright: no-info-found
License: UNKNOWN

Files: tests/DebianPackages/debian_20201226T144309Z.edsp.xz
Copyright: no-info-found
License: UNKNOWN

Files: tests/applications/*
Copyright: no-info-found
License: UNKNOWN

Files: tests/applications/dose-tests/distcheck_test_success.xz
Copyright: no-info-found
License: UNKNOWN

Files: tests/applications/dose-tests/opam_stack_overflow.xz
Copyright: no-info-found
License: UNKNOWN

Files: tests/opam/*
Copyright: no-info-found
License: UNKNOWN

